<?php
function is_valido($cache){
	$ultima_modificacao = filectime($cache); //pega a hora em que foi criado o arquivo de cache
	$c = time() - $ultima_modificacao;

	if ($c > 10) { //define o tempo de 10 segundos para gerar outro cache
		return false;
	}else{
		return true;
	}
}


$p = 'pagina';
if (isset($_GET['p']) && !empty($_GET['p']) && file_exists('paginas/'.$_GET['p'].'php')) {
	$p = $_GET['p'];
}

if (file_exists('caches/'.$p.'cache') && is_valido('caches/'.$p.'cache')) {
	
	require 'caches/'.$p.'cache';

}else{

	//nada entre as funções ob_start() e ob_end_clean() será impresso.
	ob_start();


	require 'paginas/'.$p.'.php';


	//A variável $html recebe todo o conteúdo entre as duas funções
	$html = ob_get_contents();


	ob_end_clean();

	//Cria o arquivo "cache.cache" e colca o conteúdo da variável $html dentro dele
	file_put_contents('caches/'.$p.'cache', $html);

	echo $html;

}
